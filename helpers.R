#Get TFs of the cliques
get_tf_clique_clust <- function(objS,fs){
  lg <- c()
  lm <- c()
  co <- objS@meta.data
  pfs <- process_fsObj(fs)
  co_pfs <- merge(co,pfs,by="Cell")
  for (i in seq(1,length(unique(co_pfs$Module)))) {
    lg <- c(lg,strsplit(co_pfs$TFs[which(co_pfs$Module == unique(co_pfs$Module)[i])],split=",")[[1]])
    lm <- c(lm,rep(unique(co_pfs$Module)[i],length(strsplit(co_pfs$TFs[which(co_pfs$Module == unique(co_pfs$Module)[i])],split=",")[[1]])))
  }
  return(data.frame("TF"=lg,"Module"=lm))
} 

#Get Genes Module
get_genes_module_clust <- function(objS,fs){
  lg <- c()
  lm <- c()
  co <- objS@meta.data
  pfs <- process_fsObj(fs)
  co_pfs <- merge(co,pfs,by="Cell")
  for (i in seq(1,length(unique(co_pfs$Module)))) {
    lg <- c(lg,strsplit(co_pfs$Genes[which(co_pfs$Module == unique(co_pfs$Module)[i])],split=",")[[1]])
    lm <- c(lm,rep(unique(co_pfs$Module)[i],length(strsplit(co_pfs$Genes[which(co_pfs$Module == unique(co_pfs$Module)[i])],split=",")[[1]])))
  }
  return(data.frame("Gene"=lg,"Module"=lm))
}

#Get summary table
get_summary_table_modules <- function(objS,fs){
  co <- objS@meta.data
  
  #Process functional states object - return a df with cells, module(s), TFs, Genes
  pfs <- process_fsObj(fs)
  co_pfs <- merge(co,pfs,by="Cell")
  
  df <- data.frame("Module"=unique(co_pfs$Module),"Subpopulation"=rep(NA,length(unique(co_pfs$Module[!is.na(co_pfs$Module)]))),"Genes"=rep(NA,length(unique(co_pfs$Module[!is.na(co_pfs$Module)]))))#,"Expression distribution"=rep(NA,length(unique(co$Module[!is.na(co$Module)]))))
  for(i in seq(1,length(df$Module))){
    df$Subpopulation[i] <- unique(as.numeric(co_pfs$funpart_clust[which(co_pfs$Module == df$Module[i])]))
    df$Genes[i] <- length(strsplit(co_pfs$Genes[which(co_pfs$Module == df$Module[i])],split=",")[[1]])
  }
  return(df)
}

#Process functional states object
#return a df with cells, module(s), TFs, Genes
process_fsObj <- function(fs){
  modID <- 1
  dft <- data.frame("Cell"=NA,"Module"=NA,"TFs"=NA,"Genes"=NA)
  for (x in seq(1,length(fs))) {
    #For each dataset
    funpartobj <- fs[[x]]
    if(length(unique(funpartobj$clust)) == 1){
      #no splitting
    }else{
      
      #Get functional states identified
      csnb <- unique(funpartobj$clust)
      
      #For each module level (split)
      lev <- names(funpartobj$cliques$Clust1)
      
      for (i in seq(1,length(lev))){
        if(lev[i] == "1|0"){
          #First level
          mo <- c("1","0") #Branch modules to test
        }else{
          #Level second and more
          tmp <- strsplit(lev[i],split="_")[[1]]
          tmp <- tmp[-c(length(tmp))]
          tmp <- paste(tmp,collapse = "_")
          mo <- c(
            paste(tmp,"1",sep="_"),
            paste(tmp,"0",sep="_")
          )
        }
        
        #If branch is a leaf = functional state
        typeMod <- list()
        if(mo[1] %in% csnb & !mo[2] %in% csnb){
          #Branch 1 is a leaf = functional state and not branch 0
          cells_branch1 <- names(funpartobj$clust[which(funpartobj$clust == mo[1])])
          typeMod[["branch1"]] <- "Direct module"
          
          group_branch0 <- csnb[which(str_detect(csnb,paste("^",mo[2],sep="")) == TRUE)] #All cluster under branch 0
          cells_branch0 <- names(funpartobj$clust[which(funpartobj$clust %in% group_branch0)])
          typeMod[["branch0"]] <- "Intermediate module"
          
        }else if(mo[2] %in% csnb & !mo[1] %in% csnb){
          #Branch 0 is a leaf = functional state and not branch1
          cells_branch0 <- names(funpartobj$clust[which(funpartobj$clust == mo[2])])
          typeMod[["branch0"]] <- "Direct module"
          
          group_branch1 <- csnb[which(str_detect(csnb,paste("^",mo[1],sep="")) == TRUE)] 
          cells_branch1 <- names(funpartobj$clust[which(funpartobj$clust %in% group_branch1)])
          typeMod[["branch1"]] <- "Intermediate module"
          
        }else if(mo[1] %in% csnb & mo[2] %in% csnb){
          #Both are direct modules
          cells_branch1 <- names(funpartobj$clust[which(funpartobj$clust == mo[1])])
          typeMod[["branch1"]] <- "Direct module"
          
          cells_branch0 <- names(funpartobj$clust[which(funpartobj$clust == mo[2])])
          typeMod[["branch0"]] <- "Direct module"
          
        }else{
          group_branch1 <- csnb[which(str_detect(csnb,paste("^",mo[1],sep="")) == TRUE)] #All cluster under branch 1
          group_branch0 <- csnb[which(str_detect(csnb,paste("^",mo[2],sep="")) == TRUE)] #All cluster under branch 0
          #No branch is a leaf
          cells_branch1 <- names(funpartobj$clust[which(funpartobj$clust %in% group_branch1)])
          cells_branch0 <- names(funpartobj$clust[which(funpartobj$clust %in% group_branch0)])
          typeMod[["branch0"]] <- "Intermediate module"
          typeMod[["branch1"]] <- "Intermediate module"
        }
        
        #MODULE 1
        #Test M1 - belongs to branch 0
        m1_branch0 <- getMeanRatioExpModule(funpartobj$data,
                                            names(funpartobj$cliques$Clust1[[lev[i]]]$C1),
                                            cells_branch0)
        #Test M1 - belongs to branch 1
        m1_branch1 <- getMeanRatioExpModule(funpartobj$data,
                                            names(funpartobj$cliques$Clust1[[lev[i]]]$C1),
                                            cells_branch1)
        
        #MODULE 2
        #Test M2 - belongs to branch 0
        m2_branch0 <- getMeanRatioExpModule(funpartobj$data,
                                            names(funpartobj$cliques$Clust1[[lev[i]]]$C2),
                                            cells_branch0)
        #Test M2 - belongs to branch 1
        m2_branch1 <- getMeanRatioExpModule(funpartobj$data,
                                            names(funpartobj$cliques$Clust1[[lev[i]]]$C2),
                                            cells_branch1)
        
        #Comparisons
        if(m1_branch0 > m1_branch1 & m2_branch0 < m2_branch1){
          #M1 belongs to branch 0 & M2 belongs to branch 1
          
          #REPORT ONLY DIRECT
          if(typeMod[["branch0"]] == "Direct module"){
            #Select the right branch/module for the results
            dft <- rbind(dft,data.frame(
              "Cell"=cells_branch0,
              "Module"=rep(paste("M",modID,sep=""),length(cells_branch0)),
              "TFs"=paste(names(funpartobj$cliques$Clust1[[lev[i]]]$C1),collapse = ","),
              "Genes"=paste(unique(unlist(funpartobj$cliques$Clust1[[lev[i]]]$C1)),collapse = ",")
            ))
            modID <- modID + 1
          }
          
          if(typeMod[["branch1"]] == "Direct module"){
            #Select the right branch/module for the results
            dft <- rbind(dft,data.frame(
              "Cell"=cells_branch1,
              "Module"=rep(paste("M",modID,sep=""),length(cells_branch1)),
              "TFs"=paste(names(funpartobj$cliques$Clust1[[lev[i]]]$C2),collapse = ","),
              "Genes"=paste(unique(unlist(funpartobj$cliques$Clust1[[lev[i]]]$C2)),collapse = ",")
            ))
            modID <- modID + 1
          }
        }else if(m1_branch1 > m1_branch0 & m2_branch1 < m2_branch0){
          
          #REPORT ONLY DIRECT
          if(typeMod[["branch0"]] == "Direct module"){
            #Select the right branch/module for the results
            dft <- rbind(dft,data.frame(
              "Cell"=cells_branch0,
              "Module"=rep(paste("M",modID,sep=""),length(cells_branch0)),
              "TFs"=paste(names(funpartobj$cliques$Clust1[[lev[i]]]$C2),collapse = ","),
              "Genes"=paste(unique(unlist(funpartobj$cliques$Clust1[[lev[i]]]$C2)),collapse = ",")
            ))
            modID <- modID + 1
          }
          
          if(typeMod[["branch1"]] == "Direct module"){
            #Select the right branch/module for the results
            dft <- rbind(dft,data.frame(
              "Cell"=cells_branch1,
              "Module"=rep(paste("M",modID,sep=""),length(cells_branch1)),
              "TFs"=paste(names(funpartobj$cliques$Clust1[[lev[i]]]$C1),collapse = ","),
              "Genes"=paste(unique(unlist(funpartobj$cliques$Clust1[[lev[i]]]$C1)),collapse = ",")
            ))
            modID <- modID + 1
          }
  
        }else{
          #Ambuiguity may arise for higher level intermediate levels, 
          #in this case check the higher cell exp ratio & attribute the modules accordingly
          rb <- c(m1_branch0,m1_branch1,m2_branch0,m2_branch1)
          #m1_b0, m1_b1, m2_b0, m2_b1
          id <- which(rb == max(rb))
          if(length(id) > 1){
            #If max is same, use min to differentiate - case arise at high levels (intermediate) with several functional cell states downstream
            minid <- which(rb == min(rb))
            if(minid == 1 | minid == 4){
              id <- id[which(!id %in% c(1,4))]
            }else{
              id <- id[which(id %in% c(1,4))]
            }
          }
          if(id == 1 | id == 4){
            #REPORT ONLY DIRECT
            if(typeMod[["branch0"]] == "Direct module"){
              #Select the right branch/module for the results
              dft <- rbind(dft,data.frame(
                "Cell"=cells_branch0,
                "Module"=rep(paste("M",modID,sep=""),length(cells_branch0)),
                "TFs"=paste(names(funpartobj$cliques$Clust1[[lev[i]]]$C1),collapse = ","),
                "Genes"=paste(unique(unlist(funpartobj$cliques$Clust1[[lev[i]]]$C1)),collapse = ",")
              ))
              modID <- modID + 1
            }
            
            if(typeMod[["branch1"]] == "Direct module"){
              #Select the right branch/module for the results
              dft <- rbind(dft,data.frame(
                "Cell"=cells_branch1,
                "Module"=rep(paste("M",modID,sep=""),length(cells_branch1)),
                "TFs"=paste(names(funpartobj$cliques$Clust1[[lev[i]]]$C2),collapse = ","),
                "Genes"=paste(unique(unlist(funpartobj$cliques$Clust1[[lev[i]]]$C2)),collapse = ",")
              ))
              modID <- modID + 1
            }
          }else if(id == 2 | id == 3){
            #Module 1 = branch 1 & Module 2 = branch 0
            #M1 belongs to branch 1 & M2 belongs to branch 0
            #REPORT ONLY DIRECT
            if(typeMod[["branch0"]] == "Direct module"){
              #Select the right branch/module for the results
              dft <- rbind(dft,data.frame(
                "Cell"=cells_branch0,
                "Module"=rep(paste("M",modID,sep=""),length(cells_branch0)),
                "TFs"=paste(names(funpartobj$cliques$Clust1[[lev[i]]]$C2),collapse = ","),
                "Genes"=paste(unique(unlist(funpartobj$cliques$Clust1[[lev[i]]]$C2)),collapse = ",")
              ))
              modID <- modID + 1
            }
            
            if(typeMod[["branch1"]] == "Direct module"){
              #Select the right branch/module for the results
              dft <- rbind(dft,data.frame(
                "Cell"=cells_branch1,
                "Module"=rep(paste("M",modID,sep=""),length(cells_branch1)),
                "TFs"=paste(names(funpartobj$cliques$Clust1[[lev[i]]]$C1),collapse = ","),
                "Genes"=paste(unique(unlist(funpartobj$cliques$Clust1[[lev[i]]]$C1)),collapse = ",")
              ))
              modID <- modID + 1
            }
          }
        }
      }
    }
  }
  dft <- dft[!is.na(dft$Cell),]
  return(dft)
}

#Get mean percentage cell expression ratio for TFs of the module
#m = single cell gene expression matrix
#moduletfs = tfs names
#cells = cell name of the specific cluster
#return mean cell expression ratio
getMeanRatioExpModule <- function(m,moduletfs,cells){
  m <- as.matrix(m)
  m <- m[which(rownames(m) %in% moduletfs),which(colnames(m) %in% cells)]
  mb <- m
  mb[mb>0] <- 1
  mb[mb<0] <- 0
  r <- rowSums(mb)/ncol(mb)
  return(mean(r))
}

#Get module summary: Module number, TFs, number of genes directly connected
get_modules_summary <- function(objS,fs){
  co <- objS@meta.data
  pfs <- process_fsObj(fs)
  co_pfs <- merge(co,pfs,by="Cell")
  
  df <- data.frame("Module"=unique(co_pfs$Module),"TFs"=rep(NA,length(unique(co_pfs$Module))),"Genes"=rep(NA,length(unique(co_pfs$Module))))#,"Expression distribution"=rep(NA,length(unique(co$Module[!is.na(co$Module)]))))
  for(i in seq(1,length(df$Module))){
    df$TFs[i] <- unique(co_pfs$TFs[which(co_pfs$Module == df$Module[i])])
    df$Genes[i] <- length(strsplit(co_pfs$Genes[which(co_pfs$Module == df$Module[i])],split=",")[[1]])
  }
  return(df)
}

#Get table clust: cluster number, number of cells, tissue, time, infection type
get_table_clust <- function(objS){
  co <- objS@meta.data
  df <- data.frame("Clust"=seq(1,max(unique(co$funpart_clust))))
  for (i in seq(1,length(df$Clust))) {
    df$nb[i] <- length(co$Cell[which(co$funpart_clust == df$Clust[i])])
    df$tissue[i] <- paste(unique(co$tissue[which(co$funpart_clust == df$Clust[i])]),collapse = ",")
    df$time[i] <- paste(unique(co$time[which(co$funpart_clust == df$Clust[i])]),collapse=", ")
    df$infection[i] <- paste(unique(co$pathogen[which(co$funpart_clust == df$Clust[i])]),collapse=", ")
  }
  colnames(df) <- c("Functional State","Cells","Tissue","Time","Pathogen")
  return(df)
}

