![CatalogusImmuneMuris](pictures/CIM_logo.png) 

# Catalogus Immune Muris

![version](https://img.shields.io/badge/Catalogus%20Immune%20Muris-v1.0-green) ![licence](https://img.shields.io/badge/license-MIT-green)

The Catalogus Immune Muris is a ressource of 295 immune functional states identified accross 114 datasets. It contains data for six immune cell types (Macrophages, Monocytes, Natural Killers, Dendritic cells, B cells and T cells) infected by a total of twelve pathogens (virus, bacteria, parasite, fungi).

## How to use it

*The shiny app is not yet hosted on a server.*

In order to launch it:

##### 1. Download the repository

##### 2. Install the following packages:

````r
install.packages("shiny")
install.packages("Seurat")
install.packages("DT")
install.packages("shinythemes")
install.packages("pheatmap")
install.packages("stringr")
install.packages("BiocManager")
BiocManager::install("enrichplot")
````

##### 3. Go into the main directory (root, location of the file app.R)

##### 4. Run the application
*The app windows took about 10 min to open. To limit objects size stored in the repository, several computations are performed within the code.*

````r
runApp()
````

##### Overview of the Shiny Application
<div align="center">
<img src="pictures/ScreenAppli_1.png" width="800">
</div>
<div align="center">
<img src="pictures/ScreenAppli_2.png" width="800">
</div>
<div align="center">
<img src="pictures/ScreenAppli_3.png" width="800">
</div>


