library(shiny)
library(Seurat)
source("helpers.R")
library(DT)
library(shinythemes)
library(pheatmap)
library(stringr)
library(enrichplot)
options(stringsAsFactors = F)

#Load data
#UMAP params
umap_params <- readRDS("IntegratedData/UMAP_parameters.rds")

#Macrophages
d_mac <- readRDS("IntegratedData/Integrated_compressed_Macrophages.rds")
d_mac <- ScaleData(d_mac, verbose = FALSE)
d_mac <- RunPCA(d_mac, npcs = 30,verbose = FALSE)
d_mac <- RunUMAP(d_mac, reduction = "pca", dims=NULL, features = umap_params[["Macrophages"]][["genes"]], n.neighbors = umap_params[["Macrophages"]][["n.neighbors"]], min.dist = umap_params[["Macrophages"]][["min.dist"]])
DefaultAssay(d_mac) <- "RNA"
#d_mac <- ScaleData(d_mac, verbose = FALSE)
#DCs
d_dc <- readRDS("IntegratedData/Integrated_compressed_DCcells.rds")
d_dc <- ScaleData(d_dc, verbose = FALSE)
d_dc <- RunPCA(d_dc, npcs = 30,verbose = FALSE)
d_dc <- RunUMAP(d_dc, reduction = "pca", dims=NULL, features = umap_params[["DCs"]][["genes"]], n.neighbors = umap_params[["DCs"]][["n.neighbors"]], min.dist = umap_params[["DCs"]][["min.dist"]])
DefaultAssay(d_dc) <- "RNA"
#d_dc <- ScaleData(d_dc, verbose = FALSE)
#NK cells
d_nk <- readRDS("IntegratedData/Integrated_compressed_NKcells.rds")
d_nk <- ScaleData(d_nk, verbose = FALSE)
d_nk <- RunPCA(d_nk, npcs = 30,verbose = FALSE)
d_nk <- RunUMAP(d_nk, reduction = "pca", dims=NULL, features = umap_params[["NKcells"]][["genes"]], n.neighbors = umap_params[["NKcells"]][["n.neighbors"]], min.dist = umap_params[["NKcells"]][["min.dist"]])
DefaultAssay(d_nk) <- "RNA"
#d_nk <- ScaleData(d_nk, verbose = FALSE)
#T cells
d_t <- readRDS("IntegratedData/Integrated_compressed_Tcells.rds")
d_t <- ScaleData(d_t, verbose = FALSE)
d_t <- RunPCA(d_t, npcs = 30,verbose = FALSE)
d_t <- RunUMAP(d_t, reduction = "pca", dims=NULL, features = umap_params[["Tcells"]][["genes"]], n.neighbors = umap_params[["Tcells"]][["n.neighbors"]], min.dist = umap_params[["Tcells"]][["min.dist"]])
DefaultAssay(d_t) <- "RNA"
#d_t <- ScaleData(d_t, verbose = FALSE)
#B cells
d_b <- readRDS("IntegratedData/Integrated_compressed_Bcells.rds")
d_b <- ScaleData(d_b, verbose = FALSE)
d_b <- RunPCA(d_b, npcs = 30,verbose = FALSE)
d_b <- RunUMAP(d_b, reduction = "pca", dims=NULL, features = umap_params[["Bcells"]][["genes"]], n.neighbors = umap_params[["Bcells"]][["n.neighbors"]], min.dist = umap_params[["Bcells"]][["min.dist"]])
DefaultAssay(d_b) <- "RNA"
#d_b <- ScaleData(d_b, verbose = FALSE)
#Monocytes
d_mono <- readRDS("IntegratedData/Integrated_compressed_Monocytes.rds")
d_mono <- ScaleData(d_mono, verbose = FALSE)
d_mono <- RunPCA(d_mono, npcs = 30,verbose = FALSE)
d_mono <- RunUMAP(d_mono, reduction = "pca", dims=NULL, features = umap_params[["Monocytes"]][["genes"]], n.neighbors = umap_params[["Monocytes"]][["n.neighbors"]], min.dist = umap_params[["Monocytes"]][["min.dist"]])
DefaultAssay(d_mono) <- "RNA"
#d_mono <- ScaleData(d_mono, verbose = FALSE)

#Functional states details - results from FunPart
fs_mac <- readRDS("FunctionalStates/Functional_states_datasets_Macrophages.rds")
fs_dc <- readRDS("FunctionalStates/Functional_states_datasets_DCs.rds")
fs_nk <- readRDS("FunctionalStates/Functional_states_datasets_NKcells.rds")
fs_t <- readRDS("FunctionalStates/Functional_states_datasets_Tcells.rds")
fs_b <- readRDS("FunctionalStates/Functional_states_datasets_Bcells.rds")
fs_mono <- readRDS("FunctionalStates/Functional_states_datasets_Monocytes.rds")

#Functional states - enrichment details
enrich_mac <- readRDS("FunctionalStates/Enrichment_fs_Macrophages.rds")
enrich_dc <- readRDS("FunctionalStates/Enrichment_fs_DCs.rds")
enrich_nk <- readRDS("FunctionalStates/Enrichment_fs_NKcells.rds")
enrich_t <- readRDS("FunctionalStates/Enrichment_fs_Tcells.rds")
enrich_b <- readRDS("FunctionalStates/Enrichment_fs_Bcells.rds")
enrich_mono <- readRDS("FunctionalStates/Enrichment_fs_Monocytes.rds")

df_mac <- get_table_clust(d_mac)
df_dc <- get_table_clust(d_dc)
df_nk <- get_table_clust(d_nk)
df_t <- get_table_clust(d_t)
df_b <- get_table_clust(d_b)
df_mono <- get_table_clust(d_mono)

st_mac <- get_summary_table_modules(d_mac,fs_mac)
st_dc <- get_summary_table_modules(d_dc,fs_dc)
st_nk <- get_summary_table_modules(d_nk,fs_nk)
st_t <- get_summary_table_modules(d_t,fs_t)
st_b <- get_summary_table_modules(d_b,fs_b)
st_mono <- get_summary_table_modules(d_mono,fs_mono)

mg_mac <- get_genes_module_clust(d_mac,fs_mac)
mg_dc <- get_genes_module_clust(d_dc,fs_dc)
mg_nk <- get_genes_module_clust(d_nk,fs_nk)
mg_t <- get_genes_module_clust(d_t,fs_t)
mg_b <- get_genes_module_clust(d_b,fs_b)
mg_mono <- get_genes_module_clust(d_mono,fs_mono)

tf_mac <- get_tf_clique_clust(d_mac,fs_mac)
tf_dc <- get_tf_clique_clust(d_dc,fs_dc)
tf_nk <- get_tf_clique_clust(d_nk,fs_nk)
tf_t <- get_tf_clique_clust(d_t,fs_t)
tf_b <- get_tf_clique_clust(d_b,fs_b)
tf_mono <- get_tf_clique_clust(d_mono,fs_mono)

stm_mac <- get_modules_summary(d_mac,fs_mac)
stm_dc <- get_modules_summary(d_dc,fs_dc)
stm_nk <- get_modules_summary(d_nk,fs_nk)
stm_t <- get_modules_summary(d_t,fs_t)
stm_b <- get_modules_summary(d_b,fs_b)
stm_mono <- get_modules_summary(d_mono,fs_mono)

# Define UI for app that draws a histogram ----
ui <- fluidPage(title="Catalogus Immune Muris",theme = shinytheme("flatly"),

              navbarPage("Catalogus Immune Muris",
                         
                           tabPanel("Maps",

                                      
                                    fluidRow(
                                      column(6,
                                        selectInput("ct", h4("Cell type"), 
                                                    choices = list("Macrophages" = 1, "Dendritic cells" = 2,
                                                                   "NK cells" = 3, "T cells" = 4, "B cells" = 5, "Monocytes" = 6), selected = 1)),
                                        
                                        column(6,
                                               radioButtons("var", h4("Map"),
                                                     choices = list("Functional state" = 1, "Pathogen" = 2,
                                                                    "Time" = 3),selected = 1))
                                    ), #end FluidRow
           
                                    fluidRow(column(12,
                                        plotOutput("map"))
                                    ) #end FluidRow

                             ), #end TabPanel Maps
                         
                            tabPanel("Functional states",
                                       fluidRow(column(12,DT::dataTableOutput("tableCl")))
                            ), #end TabPanel Functional states
                         
                            tabPanel("Gene Modules",

                                       tabsetPanel(
                                         
                                         type = "tabs",
                                         
                                            tabPanel(
                                           
                                              "Summary",
                                              fluidRow(column(12,
                                                              DT::dataTableOutput("summaryTable")))#,
                                           
                                            ), #end TabPanel summary
                                         
                                            tabPanel(
                                           
                                              "Transcription factors",
                                              fluidRow(
                                                column(6,
                                                       DT::dataTableOutput("TFtable")
                                                ),
                                                column(6,
                                                       plotOutput("TFmap")
                                                )
                                              ) #enf FluidRow
                                              
                                            ), #end TabPanel TF
                                         
                                            tabPanel(
                                           
                                              "Targets",
                                           
                                              fluidRow(
                                                column(6,
                                                       DT::dataTableOutput("Genetable")
                                                ),
                                                column(6,
                                                       plotOutput("Genemap")
                                                )
                                              )#end FluidRow
                                            
                                            ), #end TabPanel Targets
                                         
                                         tabPanel(
                                           
                                           "Functional annotations",
                                           
                                           fluidRow(
                                             column(6,
                                                    DT::dataTableOutput("Modules")
                                             ),
                                             column(6,
                                                    plotOutput("EnrichmentPlot")
                                             )
                                          )
                                           
                                         ) #end TabPanel Functional Annotations
                                      ) #end TabSetPanel
                              ), #end TabPanel Gene Modules
                         
                              tabPanel("About",
                                  h4("Catalogus Immune Muris is an Atlas of mouse immune responses to different pathogens: it contains 295 functional states. More details can be found in our publication: A Catalogus Immune Muris of the mouse immune responses to diverse pathogens - Manuscript submitted.")
                              ) #end TabPanel About
                         
                  ) #end NavBarPage
)

# Define server logic required to draw a histogram ----
server <- function(input, output) {
  
  #Render map
  output$map <- renderPlot({
    #Switch data depending on the selected cell type
    data <- switch(input$ct, 
                   "1" = d_mac,
                   "2" = d_dc,
                   "3" = d_nk,
                   "4" = d_t,
                   "5" = d_b,
                   "6" = d_mono)
    #Switch group.by parameter depending on the seleted variable
    gb <- switch(input$var, 
                 "1" = "funpart_clust",
                 "2" = "pathogen",
                 "3" = "time")
    #Plot the UMAP
    DimPlot(data, reduction = "umap", group.by = gb)

    })
  
  #Render data.table
  output$tableCl = DT::renderDataTable({
    
    #Switch dataframe depending on the selected cell type
    df <- switch(input$ct, 
                   "1" = df_mac,
                   "2" = df_dc,
                   "3" = df_nk,
                   "4" = df_t,
                   "5" = df_b,
                 "6" = df_mono)
    
      datatable( data = df
                 , extensions = 'Buttons'
                 , options = list( 
                   dom = "Blfrtip"
                   , buttons = 
                     list("copy", list(
                       extend = "collection"
                       , buttons = c("csv", "excel", "pdf")
                       , text = "Download"
                     ) )
                 )
      ) 
  })
  
  #Render data.table TFs
  output$TFtable = DT::renderDataTable({
    
    #Switch dataframe depending on the selected cell type
    df <- switch(input$ct, 
                 "1" = tf_mac,
                 "2" = tf_dc,
                 "3" = tf_nk,
                 "4" = tf_t,
                 "5" = tf_b,
                 "6" = tf_mono)
    
    datatable(data = df) 
  }, server = FALSE)
  
  #Render map with selected TF
  output$TFmap = renderPlot({
    #Switch data depending on the selected cell type
    data <- switch(input$ct, 
                   "1" = d_mac,
                   "2" = d_dc,
                   "3" = d_nk,
                   "4" = d_t,
                   "5" = d_b,
                   "6" = d_mono)
    
    df <- switch(input$ct, 
                 "1" = tf_mac,
                 "2" = tf_dc,
                 "3" = tf_nk,
                 "4" = tf_t,
                 "5" = tf_b,
                 "6" = tf_mono)
    
    ind <- input$TFtable_rows_selected
    if (length(ind)) {
      g <- df$TF[ind]
      FeaturePlot(data,features = g,reduction = "umap",slot = "scale.data")
    }
    
  })
  
  #Render data.table Genes
  output$Genetable = DT::renderDataTable({
    
    #Switch dataframe depending on the selected cell type
    df <- switch(input$ct, 
                 "1" = mg_mac,
                 "2" = mg_dc,
                 "3" = mg_nk,
                 "4" = mg_t,
                 "5" = mg_b,
                 "6" = mg_mono)
    
    datatable(data = df) 
  }, server = FALSE)
  
  #Render map with selected Gene
  output$Genemap = renderPlot({
    #Switch data depending on the selected cell type
    data <- switch(input$ct, 
                   "1" = d_mac,
                   "2" = d_dc,
                   "3" = d_nk,
                   "4" = d_t,
                   "5" = d_b,
                   "6" = d_mono)
    
    df <- switch(input$ct, 
                 "1" = mg_mac,
                 "2" = mg_dc,
                 "3" = mg_nk,
                 "4" = mg_t,
                 "5" = mg_b,
                 "6" = mg_mono)
    
    ind <- input$Genetable_rows_selected
    if (length(ind)) {
      g <- df$Gene[ind]
      FeaturePlot(data,features = g,reduction = "umap",slot = "scale.data")
    }
    
  })
  
  #Render data.table summary table
  output$summaryTable = DT::renderDataTable({
    
    #Switch dataframe depending on the selected cell type
    st <- switch(input$ct, 
                 "1" = st_mac,
                 "2" = st_dc,
                 "3" = st_nk,
                 "4" = st_t,
                 "5" = st_b,
                 "6" = st_mono)
    
    datatable( data = st
               , extensions = 'Buttons'
               , options = list( 
                 dom = "Blfrtip"
                 , buttons = 
                   list("copy", list(
                     extend = "collection"
                     , buttons = c("csv", "excel", "pdf")
                     , text = "Download"
                   ) )
               )
    ) 
  })
  
  #Render data.table summary modules
  output$Modules = DT::renderDataTable({
    
    #Switch dataframe depending on the selected cell type
    df <- switch(input$ct, 
                 "1" = stm_mac,
                 "2" = stm_dc,
                 "3" = stm_nk,
                 "4" = stm_t,
                 "5" = stm_b,
                 "6" = stm_mono)
    
    datatable(data = df) 
  }, server = FALSE)
  
  #Render enrichment plot with selected module
  output$EnrichmentPlot = renderPlot({
    #Switch data depending on the selected module
    df <- switch(input$ct, 
                 "1" = enrich_mac,
                 "2" = enrich_dc,
                 "3" = enrich_nk,
                 "4" = enrich_t,
                 "5" = enrich_b,
                 "6" = enrich_mono)
    
    dt <- switch(input$ct, 
                 "1" = stm_mac,
                 "2" = stm_dc,
                 "3" = stm_nk,
                 "4" = stm_t,
                 "5" = stm_b,
                 "6" = stm_mono)
    
    ind <- input$Modules_rows_selected
    if (length(ind)) {
      mo <- dt$Module[ind]
      dotplot(df[[mo]], showCategory=30) + ggtitle("Biological Immune Processes")
    }
    #p2 <- dotplot(df[[mo]][[2]], showCategory=30) + ggtitle("Metacore Pathways") - later on
    #plot_grid(p1, p2, ncol=2)
  })

}

shinyApp(ui = ui, server = server)